title: Distributing Guix System Pinebook Pro images
date: 2021-08-29 10:00
tags: gnu, guix, scheme, guile, image, pinebook pro
summary: Distributing Guix System Pinebook Pro images
---

A few months ago, Enrico Schwass very generously offered to ship me a Pinebook
Pro so that I can port Guix System on it. I finally took the time to take it
out of the drawer, where it was quietly expecting a travel to the land of
parenthesis.

There were previous efforts in the past to add support for that
machine. Janneke managed to install Guix System from Debian and documented it
in a nice blog
[post](https://joyofsource.com/guix-system-on-the-pinebook-pro.html). Vagrant
Cascadian built up on that work so that the default Guix System ARM64 Linux
kernel can support the Pinebook Pro, as summarized
[here](https://lists.gnu.org/archive/html/guix-devel/2021-05/msg00032.html).

On my side, I worked on improving the Guix System image generation mechanism,
notably under the impulsion of the
[Hurd](https://guix.gnu.org/en/blog/2020/childhurds-and-substitutes/) work
driven by Janneke. You can have a look to the blog
[post](https://othacehe.org/the-guix-system-image-api.html) I wrote at the
time.

That new image API allowed to define a Pinebook Pro image type and a barebones
Pinebook Pro image in the Guix
[sources](https://git.savannah.gnu.org/cgit/guix.git/tree/gnu/system/images/pinebook-pro.scm).

This means that one can run:

```sh
guix system image gnu/system/images/pinebook-pro.scm
...
successfully built /gnu/store/m82b13hq40fv45895hfsd7hak4cch51z-pinebook-pro-barebones-raw-image.drv
/gnu/store/0gq2qqx5npmx94hdj1l059b7kf1cz68g-pinebook-pro-barebones-raw-image
```

to produce a raw barebones Guix System image targeting the Pinebook Pro. The
only work left is to copy this image on a micro SD-card, this way:

```sh
dd if=/gnu/store/0gq2qqx5npmx94hdj1l059b7kf1cz68g-pinebook-pro-barebones-raw-image of=/dev/mmcblk0 bs=4M
```

When the micro SD-card is inserted in the Pinebook Pro, it will automatically
boot on it and display the long-awaited login shell.

![Guix System on the Pinebook Pro](/static/pinebook-pro.jpg)

If you do not wish to build the Pinebook Pro image yourself, you can also
download it from the Guix [website](https://guix.gnu.org/en/download/latest/)
in the **Download** section. This image is built daily by the
[Cuirass](https://guix.gnu.org/cuirass/) continuous integration system.

![Pinebook Pro image download](/static/pinebook-download.png)

## Future steps

The next logical step is to provide a Guix System Pinebook Pro image with some
lightweight desktop environment such as Xfce.

While technically this should be as simple as adding the following line in the
pinebook-pro.scm operating-system declaration:

```scheme
(services (cons (service xfce-desktop-service-type)
                 %base-services))
```

it is a little bit more challenging in practice.

By default, the Pinebook Pro image is cross-compiled to the ARM64
architecture. Guix does not support cross-compilation for all the build
systems yet. On the bright side, Maxime Devos recently added cross-compilation
support for the
[meson](https://lists.gnu.org/archive/html/guix-patches/2021-07/msg00923.html)
build system, and is currently working on the
[glib-or-gtk](https://lists.gnu.org/archive/html/guix-patches/2021-08/msg01074.html)
build system.

Another possibility would be to use the QEMU transparent emulation
[mechanism](https://guix.gnu.org/manual/en/html_node/Virtualization-Services.html)
to build the image by emulating an ARM64 architecture on an Intel x86_64
machine. The problem with that approach is that compilation is terribly slow
and sometimes fails due to emulation limits. This is nonetheless the approach
that I will probably take in the short term.

Thanks again to Enrico for his support, keep tuned!
