title: Building your own channels
date: 2021-04-02 10:00
tags: gnu, guix, cuirass, scheme, guile, avahi, discovery, substitute, channels
summary: Building your own channels
---

Like many GNU Guix users, I'm using multiple Guix
[channels](https://guix.gnu.org/manual/en/html_node/Channels.html).

Let say I'm using this `~/.config/guix/channels.scm` file:

```scheme
(cons* (channel
        (name 'flat)
        (url "https://github.com/flatwhatson/guix-channel.git")
        (introduction
         (make-channel-introduction
          "33f86a4b48205c0dc19d7c036c85393f0766f806"
          (openpgp-fingerprint
           "736A C00E 1254 378B A982  7AF6 9DBE 8265 81B6 4490"))))
       %default-channels)
```

The official GNU Guix [build farm](https://ci.guix.gnu.org) is only building
the default Guix channel.  It means that the packages provided by the `flat`
channel won't have any available substitutes.

On my local network, I have a few laptops without much processing power, and a
powerful desktop.  The ideal setup would be to have the powerful desktop build
all the packages of the `flat` channel, each time there is a new commit either
in the default `guix` channel or in the `flat` channel.

Then, the substitutes built by the desktop machine would be made available on
the local network.

The good news is that using the new release of
[Cuirass](https://guix.gnu.org/en/blog/2021/cuirass-10-released/), the GNU
Guix continuous integration software, as well as the recent substitute server
discovery [mechanism](https://othacehe.org/substitute-server-discovery.html),
this should be relatively easy.

Let's see how it can be done, step by step.

## The desktop machine configuration

We need to setup two specific services on the desktop machine:

- A Cuirass service to build the `flat` channel.

- A Guix publish service to share the package substitutes.

```scheme
(service cuirass-service-type
         (cuirass-configuration
          (specifications #~'())
          (host "0.0.0.0")))
(service guix-publish-service-type
         (guix-publish-configuration
          (host "0.0.0.0")
          (advertise? #t)))
```

Here we pass an empty specifications list to the Cuirass service, as we will
use the Web interface to configure it later on.

We also configure the Guix publish service to advertise itself on the local
network using Avahi.

## The flat specification creation

It's time to configure Cuirass to build the `flat` channel packages.  Let's do
it through the Web interface by visiting: `http://my-desktop:8081`.

Then by clicking on the "+" button, we can add a specification.

![flat spec](/static/flat-spec.png)

We configure Cuirass to operate on two channels: the default `guix` channel on
the `master` branch, and the `flat` channel also on the `master` branch.

Here we use the `channels` build type with the `flat` parameter.  It means
that we ask Cuirass to build only the packages that are part of the `flat`
channel.  You can read the Cuirass documentation for more information on the
[specification](https://guix.gnu.org/cuirass/manual/html_node/Specifications.html#Specifications)
format.

We choose to build those packages for the `x86_64-linux` architecture with the
default priority.

Cuirass will now start building the latest version of the `flat` channel
packages.

![flat evaluation](/static/flat-eval.png)

If your are allergic to web interfaces, here's how to configure Cuirass to
achieve the same result:

```scheme
(service cuirass-service-type
         (cuirass-configuration
          (specifications
           #~(list
              (specification
               (name "flat")
               (build '(channels flat))
               (channels
                (cons (channel
                       (name 'flat)
                       (url "https://github.com/flatwhatson/guix-channel"))
                      %default-channels)))))
          (host "0.0.0.0")))
```

### The laptops configuration

There's one missing piece.  We need to configure the laptops to use the
substitutes built on the desktop machine.

```scheme
(guix-configuration
 (discover? #t)
 (authorized-keys
  (append (list (local-file "./keys/elbruz.scm"))
           %default-authorized-guix-keys)))
```

It can be done by setting the `discover?` option of the Guix daemon service to
`true`.  We also need to authorize the key of the desktop machine.

Now, if we run `guix build emacs-native-comp` we do not need to build stuff
anymore.

```bash
mathieu@meije ~$ guix build emacs-native-comp
The following file will be downloaded:
   /gnu/store/263zbli2cq3zbwcmsc4n27kb7zlzxrfj-emacs-native-comp-28.0.50-149.8d55070
substituting /gnu/store/263zbli2cq3zbwcmsc4n27kb7zlzxrfj-emacs-native-comp-28.0.50-149.8d55070...
downloading from http://192.168.1.51/nar/gzip/263zbli2cq3zbwcmsc4n27kb7zlzxrfj-emacs-native-comp-28.0.50-149.8d55070 ...
 emacs-native-comp-28.0.50-149.8d55070                                14.3MiB/s 00:06 | 88.3MiB transferred
/gnu/store/7wiv7jk52ix14zx1dk2bdnvnm07zrh41-emacs-native-comp-28.0.50-149.8d55070
```

You can replicate this setup at home, and even simplify it by running Cuirass
directly on your main machine.  You can also configure Cuirass to build
specific packages or manifests.

Don't hesitate to ask for help on `#guix` channel or on `help-guix@gnu.org` to
configure your own Cuirass service.
