(use-modules (haunt asset)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
             (haunt html)
             (haunt page)
             (haunt post)
             (haunt reader)
             (haunt reader commonmark)
             (haunt site)
             (haunt utils)
             (commonmark)
             (syntax-highlight)
             (syntax-highlight scheme)
             (syntax-highlight xml)
             (syntax-highlight c)
             (sxml match)
             (sxml transform)
             (srfi srfi-19)
             (ice-9 match))

(define (maybe-highlight-code lang source)
  (let ((lexer (match lang
                 ('scheme lex-scheme)
                 ('xml    lex-xml)
                 ('c      lex-c)
                 (_ #f))))
    (if lexer
        (highlights->sxml (highlight lexer source))
        source)))

(define (sxml-identity . args) args)

(define (highlight-code . tree)
  (sxml-match tree
    ((code (@ (class ,class) . ,attrs) ,source)
     (let ((lang (string->symbol
                  (string-drop class (string-length "language-")))))
       `(code (@ ,@attrs)
             ,(maybe-highlight-code lang source))))
    (,other other)))

(define %commonmark-rules
  `((code . ,highlight-code)
    (*text* . ,(lambda (tag str) str))
    (*default* . ,sxml-identity)))

(define (post-process-commonmark sxml)
  (pre-post-order sxml %commonmark-rules))

(define commonmark-reader*
  (make-reader (make-file-extension-matcher "md")
               (lambda (file)
                 (call-with-input-file file
                   (lambda (port)
                     (values (read-metadata-headers port)
                             (post-process-commonmark
                              (commonmark->sxml port))))))))

(define (stylesheet name)
  `(link (@ (rel "stylesheet")
            (href ,(string-append "/css/" name ".css")))))

(define* (anchor content #:optional (uri content))
  `(a (@ (href ,uri)) ,content))

(define (link name uri)
  `(a (@ (href ,uri)) ,name))

(define %cc-by-sa-link
  '(a (@ (href "https://creativecommons.org/licenses/by-sa/4.0/"))
      "Creative Commons Attribution Share-Alike 4.0 International"))

(define %cc-by-sa-button
  '(a (@ (class "cc-button")
         (href "https://creativecommons.org/licenses/by-sa/4.0/"))
      (img (@ (src "https://licensebuttons.net/l/by-sa/4.0/80x15.png")))))

(define (first-paragraph post)
  (let loop ((sxml (post-sxml post))
             (result '()))
    (match sxml
      (() (reverse result))
      ((or (('p ...) _ ...) (paragraph _ ...))
       (reverse (cons paragraph result)))
      ((head . tail)
       (loop tail (cons head result))))))

(define my-theme
  (theme #:name "my-theme"
         #:layout
         (lambda (site title body)
           `((doctype "html")
             (head
              (meta (@ (charset "utf-8")))
              (title ,title)
              ,(stylesheet "reset")
              ,(stylesheet "fonts")
              ,(stylesheet "my-theme"))
             (body
              (div (@ (class "container"))
                   (div (@ (class "nav"))
                        (ul (li ,(link "Mathieu Othacehe" "/"))
                            (li (@ (class "fade-text")) " ")
                            (li ,(link "Work" "/work.html"))
                            (li ,(link "Projects" "/projects.html"))
                            (li ,(link "Blog" "/blog.html"))))
                   ,body
                   (footer (@ (class "text-center"))
                           (p (@ (class "copyright"))
                              "© 2024 Mathieu Othacehe <othacehe@gnu.org>"
                              ,%cc-by-sa-button))))))
         #:post-template
         (lambda (post)
           `((h1 (@ (class "title")),(post-ref post 'title))
             (div (@ (class "date"))
                  ,(date->string (post-date post)
                                 "~B ~d, ~Y"))
             (div (@ (class "post"))
                  ,(post-sxml post))))
         #:collection-template
         (lambda (site title posts prefix)
           (define (post-uri post)
             (string-append "/" (or prefix "")
                            (site-post-slug site post) ".html"))

           `((h1 ,title)
             ,(map (lambda (post)
                     (let ((uri (string-append "/"
                                               (site-post-slug site post)
                                               ".html")))
                       `(div (@ (class "summary"))
                             (h2 (a (@ (href ,uri))
                                    ,(post-ref post 'title)))
                             (div (@ (class "date"))
                                  ,(date->string (post-date post)
                                                 "~B ~d, ~Y"))
                             (div (@ (class "post"))
                                  ,(first-paragraph post))
                             (a (@ (href ,uri)) "read more ➔"))))
                   posts)))))

(define %collections
  `(("Recent Blog Posts" "blog.html" ,posts/reverse-chronological)))

(define (static-page title file-name body)
  (lambda (site posts)
    (make-page file-name
               (with-layout my-theme site title body)
               sxml->html)))

(define index-page
  (static-page
   "Mathieu Othacehe"
   "index.html"
   `((div (@ (style "overflow: hidden"))
          (div (@ (class "index-image"))
               (img (@ (src "/static/mo.jpeg") (width "90%"))))
          (div (@ (class "index-text"))
               (h2 "Hi.")
               (p "I am a free software developper, an amateur
rock-climber and mountaineer based in Annecy, France.")
               (p "I have an engineering degree in computer science and
embedded systems from " ,(anchor "INSA
Toulouse" "http://www.insa-toulouse.fr/fr/index.html")
".")
               (p "I am the CEO of "
                  (a (@ (href "https://root2peak.com")) "Root to Peak")
                  ", a company that can help you
 introduce Linux and open-source software in your embedded products
 and projects. Find more about it "
                  (a (@ (href "/work.html")) "here."))
               (p "In my free time, I'm mostly focused on maintaining "
                  ,(anchor "GNU Guix" "https://guix.gnu.org/")
                  ", the " ,(anchor "GNU
Guile" "https://www.gnu.org/software/guile/") " ecosystem and extending GNU
Guix support to a wider range of machines and architectures.")))
     (h2 "Contact.")
     (p "Mail — "
        ,(anchor "othacehe@gnu.org" "mailto:othacehe@gnu.org"))
     (p "PGP — "
        ,(anchor "4008 6A7E 0252 9B60 31FB  8607 8354 7635 3176 9CA6"
                 "/static/othacehe.asc")))))

(define work-page
  (static-page
   "Work"
   "work.html"
   `((h1 "Embedded Linux services")
     (div (@ (style "overflow:hidden"))
          (div (@ (class "work-image"))
               (a (@ (href "https://root2peak.com"))
                  (img (@ (src "/static/work.jpg")
                          (width "75%")
                          (style "margin-left: 2em")))))
          (div
           (@ (class "work-text"))
           (p "I am the CEO of the "
              (a (@ (href "https://root2peak.com"))
                 "Root to Peak")
              " company. We offer services around: "
              (ul
               (li "SoC selection and hardware review")
               (li "BSP creation with Yocto")
               (li "Linux kernel drivers and open-source project mainlining")
               (li "Software and architecture audit review")
               (li "Embedded Linux trainings")
               (li "Continuous integration using Gitlab CI & docker")))
           (p "Contact us:")
           (ul
            (li "Mail: " ,(anchor "contact@root2peak.com"
                                  "mailto:contact@root2peak.com"))
            (li "Phone: +33 689 588 134")))))))

(define projects-page
  (static-page
   "Projects"
   "projects.html"
   `((h1 "Projects.")
     (p ,(anchor "GNU Guix" "https://guix.gnu.org/")
        " — A distribution of the GNU operating system centered on the GNU
Guix package manager. Co-maintainer of the project.")
     (p ,(anchor "Linux" "https://www.kernel.org/")
        " — Author of a few USB and IIO device drivers.")
     (p ,(anchor "U-Boot" "https://github.com/u-boot/u-boot")
        " — Contributions around i.MX93 support.")
     (p ,(anchor "Cuirass"
                 "https://git.savannah.gnu.org/cgit/guix/guix-cuirass.git")
        " — Contributor, continuous integration tool for Guix.")
     (p ,(anchor "Ardupilot" "https://ardupilot.org/")
        " — Open Autopilot software for drones and other autonomous systems.
Worked on the integration of Parrot Disco delta-wing.")
     (p ,(anchor "Guile-Git" "https://gitlab.com/guile-git/guile-git")
        " — Contributor, Guile bindings for libgit2.")
     (p ,(anchor "Guile-Parted" "https://gitlab.com/mothacehe/guile-parted")
        " — Founder, Guile bindings for Parted.")
     (p ,(anchor "Guile-Newt" "https://gitlab.com/mothacehe/guile-newt")
        " — Founder, Guile bindings for Newt.")
     (h1 "Talks.")
     (p ,(anchor "Fund the Code 2019"
                 "https://player.vimeo.com/video/329759664")
        " — Présentation de GNU Guix avec Clément Lassieur.")
     (p ,(anchor "Fosdem 2020"
                 "https://fosdem.org/2020/schedule/event/ggaaattyp/")
        " — GNU Guix as an alternative to the Yocto Project.")
     (p ,(anchor "Fosdem 2022"
                 "https://archive.fosdem.org/2023/schedule/event/hw_journey/")
        " — A journey to the hardware world.")
     (h1 "Articles.")
     (p ,(anchor "Porting GuixSD to ARMv7"
                "https://guix.gnu.org/blog/2017/porting-guixsd-to-armv7/"))
     (p ,(anchor "Childhurds and GNU/Hurd substitutes"
                "https://guix.gnu.org/en/blog/2020/childhurds-and-substitutes/"))
     (p ,(anchor "Cuirass 1.0 released"
                "https://guix.gnu.org/en/blog/2021/cuirass-10-released/")))))

(site #:title "Othacehe"
      #:domain "othacehe.org"
      #:build-directory "/tmp/website"
      #:default-metadata
      '((author . "Mathieu Othacehe")
        (email  . "othacehe@gnu.org"))
      #:readers (list commonmark-reader*)
      #:builders (list (blog #:theme my-theme
                             #:collections %collections)
                       (atom-feed)
                       (atom-feeds-by-tag)
                       index-page
                       projects-page
                       work-page
                       (static-directory "css")
                       (static-directory "fonts")
                       (static-directory "static")))
